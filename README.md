# Hardcore Wither
The more Withers you fight, the harder the fight gets.

Withers spawned around you get increasingly difficult special abilities. If a Wither is spawned with multiple players around, all of their experience with Withers is taken into account.

All abilities can be disabled/adjusted in configs.

There are plans for more special abilities in the future, suggestions are welcome (issues submitted to the repository are perfered).

Feel free to use in modpacks; notification is appreciated, but not required.

## Building/Working with the code

Both Eclipse and IntelliJ can import Gradle projects.