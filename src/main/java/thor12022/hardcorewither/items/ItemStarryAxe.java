package thor12022.hardcorewither.items;

import java.util.List;

import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import thor12022.hardcorewither.HardcoreWither;
import thor12022.hardcorewither.ModInformation;
import thor12022.hardcorewither.config.Config;
import thor12022.hardcorewither.config.Configurable;
import thor12022.hardcorewither.util.I18n;

@Configurable
public class ItemStarryAxe extends ItemAxe implements IItem
{
   private static final String NAME = "starryAxe";

   @Config()
   private static float attackingSpeed = -3.0F;

   @Config()
   private static float damageMultiplier = 1.5f;
   
   @Config
   private boolean isEnabled = true;
   
   public ItemStarryAxe()
   {
      super(MaterialHelper.witherEmerald, MaterialHelper.witherEmerald.getDamageVsEntity() * damageMultiplier, attackingSpeed);
      setUnlocalizedName(ModInformation.ID + "." + NAME);
      this.setCreativeTab(HardcoreWither.CREATIVE_TAB);
      HardcoreWither.CONFIG.register(this);
   }
   
   @Override
   @SideOnly(Side.CLIENT)
   public boolean hasEffect(ItemStack stack)
   {
       return true;
   }
   
   @Override
   @SideOnly(Side.CLIENT)
   public void addInformation (ItemStack stack, EntityPlayer player, List<String> list, boolean par4)
   {
      list.add(TextFormatting.GRAY.toString() + TextFormatting.ITALIC + I18n.localize("tooltip." + ModInformation.ID + ".unbreaking"));
   }
   
   @Override
   public int getMaxDamage()
   {
       return 0;
   }  
   
   @Override
   public boolean isDamageable()
   {
      return false;
   }
   
   @Override
   public boolean isItemTool(ItemStack stack)
   {
      return stack.getItem() instanceof ItemStarryAxe;
   }
   @Override
   public String name()
   {
      return NAME;
   }

   @Override
   public void registerItem()
   {
      setRegistryName(NAME);
      GameRegistry.register(this);
   }

   @Override
   public void registerModel()
   {
      ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(ModInformation.ID + ":" + NAME));
   }
   
   @Override
   public void registerRecipe()
   {
        GameRegistry.addRecipe(  new ItemStack(this),
                                 "EE ",
                                 "ES ",
                                 " S ",
                                 'E', new ItemStack(ItemRegistry.craftingItem, 1, ItemCrafting.META_STARRY_EMERALD),
                                 'S', new ItemStack(ItemRegistry.craftingItem, 1, ItemCrafting.META_STARRY_STICK));
   }

   @Override
   public boolean isEnabled()
   {
      return isEnabled;
   }
}
