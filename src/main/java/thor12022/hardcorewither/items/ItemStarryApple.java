package thor12022.hardcorewither.items;

import thor12022.hardcorewither.HardcoreWither;
import thor12022.hardcorewither.ModInformation;
import thor12022.hardcorewither.config.Config;
import thor12022.hardcorewither.config.Configurable;
import thor12022.hardcorewither.potions.PotionRegistry;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.MobEffects;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@Configurable
public class ItemStarryApple extends ItemFood implements IItem
{
   @Config(minInt = 0)
   private static int antiWitherDurationTicks = 2400;
   
   @Config(minInt = 0)
   private static int absorptionDurationTicks = 1200;
   
   @Config(minInt = 0)
   private static int regenDurationTicks = 900;
   
   @Config(minInt = 0)
   private static int resistanceDurationTicks = 900;
   
   @Config(minInt = 0)
   private static int strengthDurationTicks = 900;
   
   @Config(minInt = 0)
   private static int antiWitherLevel = 1;
   
   @Config(minInt = 0)
   private static int absorptionLevel = 1;
   
   @Config(minInt = 0)
   private static int regenLevel = 2;
   
   @Config(minInt = 0)
   private static int resistanceLevel = 2;
   
   @Config(minInt = 0)
   private static int strengthLevel = 2;
   
   @Config(minInt = 0)
   private static int instaHealLevel = 1;
   
   @Config
   private static boolean enableStarryApple = true;
   
   static
   {
      HardcoreWither.CONFIG.register(ItemStarryApple.class);
   }
   
   private final static String NAME = "starryApple";
      
   public ItemStarryApple()
   {
      super(4, 1.2F, false);
      setUnlocalizedName(ModInformation.ID + "." + NAME);
      setCreativeTab(HardcoreWither.CREATIVE_TAB);
      this.setAlwaysEdible();
   }
   
   @Override
   @SideOnly(Side.CLIENT)
   public boolean hasEffect(ItemStack stack)
   {
       return true;
   }

   @Override
   public void onFoodEaten(ItemStack stack, World world, EntityPlayer player)
   {
       if (!world.isRemote)
       {
          player.addPotionEffect(new PotionEffect(PotionRegistry.potionAntiWither,	antiWitherDurationTicks,   antiWitherLevel - 1));
          player.addPotionEffect(new PotionEffect(MobEffects.ABSORPTION,            absorptionDurationTicks,   absorptionLevel - 1));
          player.addPotionEffect(new PotionEffect(MobEffects.REGENERATION,          regenDurationTicks,        regenLevel - 1));
          player.addPotionEffect(new PotionEffect(MobEffects.RESISTANCE,            resistanceDurationTicks,   resistanceLevel - 1));
          player.addPotionEffect(new PotionEffect(MobEffects.STRENGTH,           	strengthDurationTicks,     strengthLevel - 1));
          player.addPotionEffect(new PotionEffect(MobEffects.REGENERATION,          0,                         instaHealLevel - 1));
       }
   }

   @Override
   public String name()
   {
      return NAME;
   }

   @Override
   public void registerItem()
   {
      setRegistryName(NAME);
      GameRegistry.register(this);
   }

   @Override
   public void registerModel()
   {
      ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(ModInformation.ID + ":" + NAME));
   }

   @Override
   public void registerRecipe()
   {
      GameRegistry.addShapedRecipe(new ItemStack(this), new Object[]{
         " s ", 
         "sas", 
         " s ", 
         'a', Items.APPLE, 
         's', Items.NETHER_STAR});
   }

   @Override
   public boolean isEnabled()
   {
      return enableStarryApple;
   }
}
