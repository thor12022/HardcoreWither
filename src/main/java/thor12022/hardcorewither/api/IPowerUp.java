package thor12022.hardcorewither.api;

import javax.annotation.Nonnull;

import net.minecraft.entity.boss.EntityWither;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.registry.IForgeRegistryEntry;

/**
 * New Power Ups should extend {@link IPowerUp.Impl}
 */
public interface IPowerUp extends IForgeRegistryEntry<IPowerUp>
{   
   /**
    * Power Up action to be performed on tick
    * @param data value returned from getPowerUpEffect
    */
   void updateWither(IPowerUpEffect data);
   
   /**
    * Power Up action to perform when the Wither dies
    * @param data value returned from getPowerUpEffect 
    */
   void witherDied(IPowerUpEffect data);
      
   /**
    * Initializes the Power Up
    * @param wither the affected Wither
    * @param strength the strength of the Power Up
    * @post this will apply the PowerUp regardless of min Wither Level and Max Strength
    * @return an object representing state data for this Power Up, actual type needs only be known by PowerUp itself
    * @note return value should contain all necessary info, including wither and strength data
    */
   @Nonnull IPowerUpEffect getPowerUpEffect(EntityWither wither, int strength);
   
   /**
    * Initializes the Power Up with saved state data
    * @param wither the affected Wither
    * @param data saved state NBT
    * @post this will apply the PowerUp regardless of min Wither Level and Max Strength
    * @return an object representing state data for this Power Up, actual type needs only be known by PowerUp itself
    * @note return value should contain all necessary info, including wither and strength data
    */
   @Nonnull IPowerUpEffect restorePowerUp(EntityWither wither, NBTTagCompound stateNbt);
   
   /**
    * Allows the PowerUp to not be applicable to a wither for any arbitrary reason
    * @param wither
    * @return false if this PowerUp should not apply to the supplied wither
    * @note this should be independent from the minWitherLevel and maxStrength methods, 
    *    e.g. will only work with a player nearby, or will only work in certain dimensions
    */
   boolean canApply(EntityWither wither);
   
   /**
    * Minimum level the Wither must be before this Power Up can be used
    */
   int minWitherLevel();
   
   /**
    * Maximum strength this Power Up will support
    */
   int maxStrength();
   
   /**
    * The Abstract class implementation of IPowerUp,
    *    extends IForgeRegistryEntry.Impl.
    * This is the recommend class to extend a Power Up from
    */
   public static abstract class Impl extends IForgeRegistryEntry.Impl<IPowerUp> implements IPowerUp 
   {}
}
