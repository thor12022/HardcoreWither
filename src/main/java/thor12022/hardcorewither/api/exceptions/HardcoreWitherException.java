package thor12022.hardcorewither.api.exceptions;

public class HardcoreWitherException extends Exception
{
   private static final long serialVersionUID = 1L;
   
   public HardcoreWitherException(String string)
   {
      super(string);
   }
}
