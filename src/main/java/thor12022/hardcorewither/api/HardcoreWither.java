package thor12022.hardcorewither.api;

import thor12022.hardcorewither.api.exceptions.InvalidApiUsageException;

public class HardcoreWither
{ 
   static IHardcoreWitherApi instance;
   
   public static IHardcoreWitherApi getApi() throws InvalidApiUsageException
   {
      if(instance != null)
      {
         return instance;
      }
      throw new InvalidApiUsageException("The Hardcore Wither API has not been initialized yet");
   }
}
