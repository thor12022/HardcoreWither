package thor12022.hardcorewither.api;

import net.minecraft.entity.boss.EntityWither;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.util.INBTSerializable;

/**
 * This class is used to store data needed by a PowerUp between updates
 * @todo document methods
 */
public interface IPowerUpEffect extends INBTSerializable<NBTTagCompound>
{
   public int getStrength();
   
   public void setStrength(int strength);
   
   public void updateEffect();   

   public void onDied();
   
   public IPowerUp getCreator();

   public EntityWither getWither();
}
