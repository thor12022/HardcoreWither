package thor12022.hardcorewither.wither.powerups;

import net.minecraft.entity.boss.EntityWither;
import net.minecraft.nbt.NBTTagCompound;
import thor12022.hardcorewither.api.IPowerUp;
import thor12022.hardcorewither.api.IPowerUpEffect;

public class BasePowerUpEffect implements IPowerUpEffect
{      
   protected final IPowerUp powerUp;
   protected final EntityWither wither;
   protected int strength;
     
   public BasePowerUpEffect(EntityWither wither, IPowerUp powerUp)
   {
      this.wither = wither;
      this.powerUp = powerUp;
   }
   
   @Override
   public void setStrength(int strength)
   {
      this.strength = strength;
   }
   
   @Override
   public int getStrength()
   {
      return strength;
   }
   
   @Override
   public void updateEffect()
   {
      powerUp.updateWither(this);
   }
   
   @Override
   public void onDied()
   {
      powerUp.witherDied(this);
   }
   
   @Override
   public NBTTagCompound serializeNBT()
   {
      NBTTagCompound tag = new NBTTagCompound();
      tag.setString("powerUpId", powerUp.getRegistryName().toString());
      tag.setInteger("strength", strength);
      return tag;
   }
   
   @Override
   public void deserializeNBT(NBTTagCompound nbt)
   {
      setStrength(nbt.getInteger("strength"));
   }
   
   @Override
   public IPowerUp getCreator()
   {
      return powerUp;
   }

   @Override
   public EntityWither getWither()
   {
      return wither;
   }
}
