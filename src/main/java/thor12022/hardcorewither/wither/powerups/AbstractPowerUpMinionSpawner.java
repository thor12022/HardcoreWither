package thor12022.hardcorewither.wither.powerups;

import thor12022.hardcorewither.api.IPowerUpEffect;
import thor12022.hardcorewither.config.Config;
import thor12022.hardcorewither.config.Configurable;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.MobSpawnerBaseLogic;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

@Configurable
abstract class AbstractPowerUpMinionSpawner extends BasePowerUp
{
   protected class Data extends BasePowerUpEffect
   {
      int    delay;
      int    playerRange;
      int    maxEntities;
      int    minDelay;
      int    maxDelay;
      int    spawnCount;
      int    spawnRange;
      
      MobSpawnerBaseLogic spawner = new MobSpawnerBaseLogic()
      {          
         @Override
         public void broadcastEvent(int id)
         {}

         @Override
         public World getSpawnerWorld()
         {
            return wither.worldObj;
         }

         @Override
         public BlockPos getSpawnerPosition()
         {
            return wither.getPosition();
         }
      };
      
      Data(EntityWither wither)
      {
         super(wither, AbstractPowerUpMinionSpawner.this);
      }
      
      protected void resetSpawnerToData()
      {
         NBTTagCompound nbt = new NBTTagCompound();
         nbt.setShort("Delay", (short)Math.round(defaultDelay + spawnDelayModifier * strength));
         nbt.setShort("RequiredPlayerRange", (short)defaultPlayerRange);
         nbt.setShort("MaxNearbyEntities", (short)Math.round(defaultMaxEntities + maxEntitiesModifier * strength));
         nbt.setShort("MinSpawnDelay", (short)Math.round(defaultMinDelay + spawnDelayModifier * strength));
         nbt.setShort("MaxSpawnDelay", (short)Math.round(defaultMaxDelay + spawnDelayModifier * strength));
         nbt.setShort("SpawnCount", (short)Math.round(defaultSpawnCount + spawnCountModifier  * strength));
         nbt.setShort("SpawnRange", (short)defaultSpawnRange);
         spawner.readFromNBT(nbt);
         spawner.setEntityName(entityLocalizedName);
      }

      @Override
      public NBTTagCompound serializeNBT()
      {
         NBTTagCompound nbt = super.serializeNBT();
         NBTTagCompound spawnerNbt = new NBTTagCompound();
         spawner.writeToNBT(spawnerNbt);
         nbt.setTag("spawnerLogic", spawnerNbt);
         return nbt;
      }

      @Override
      public void deserializeNBT(NBTTagCompound nbt)
      {
         NBTTagCompound spawnerNbt = nbt.getCompoundTag("spawnerLogic");
         spawner.readFromNBT(spawnerNbt);
         super.deserializeNBT(nbt);
      }

      @Override
      public void setStrength(int strength)
      {
          super.setStrength(strength);
          resetSpawnerToData();
      }
   }
   
   final protected String entityLocalizedName;
   
   @Config(maxInt = 65535)
   protected int defaultDelay = 20;
   
   @Config(maxInt = 65535)
   protected int defaultPlayerRange = 48;
   
   @Config(maxInt = 65535)
   protected int defaultMaxEntities = 6;
   
   @Config(maxInt = 65535)
   protected int defaultMinDelay = 600;
   
   @Config(maxInt = 65535)
   protected int defaultMaxDelay = 800;

   @Config(maxInt = 65535)
   protected int defaultSpawnCount = 4;

   @Config(maxInt = 65535)
   protected int defaultSpawnRange = 4;
    
   @Config(minFloat = 1f, maxFloat = 10f, comment = "Amount to increase Spawn Count by. 1.0 to never increase")
   static protected float spawnCountModifier = 1.1f;
   
   @Config(minFloat = 0f, maxFloat = 1f, comment = "The smaller it is, the faster the delay decrease. 1.0 to never decrease")
   static protected float spawnDelayModifier = 0.8f;
   
   @Config(minFloat = 1f, maxFloat = 10f, comment = "Amount to increase Max Entities by. 1.0 to never increase")
   static protected float maxEntitiesModifier = 1.1f;
   
   public AbstractPowerUpMinionSpawner(int minLevel, int maxStrength, String entityLocalizedName)
   {
      super(minLevel, maxStrength);
      this.entityLocalizedName = entityLocalizedName;
   }
   
   @Override
   public void updateWither(IPowerUpEffect data)
   {
      ((Data)data).spawner.updateSpawner();
   }

   @Override
   public void witherDied(IPowerUpEffect data)
   {}

   @Override
   public IPowerUpEffect getPowerUpEffect(EntityWither wither, int strength)
   {
      Data data = new Data(wither);
      data.setStrength(strength);
      data.resetSpawnerToData();
      return data;
   }
   
   @Override
   public IPowerUpEffect restorePowerUp(EntityWither wither, NBTTagCompound stateNbt)
   {

      Data data = new Data(wither);
      data.deserializeNBT(stateNbt);
      return data;
   }
}
