package thor12022.hardcorewither.wither.powerups;

import thor12022.hardcorewither.HardcoreWither;
import thor12022.hardcorewither.api.IPowerUpEffect;
import thor12022.hardcorewither.config.Config;
import thor12022.hardcorewither.config.Configurable;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.nbt.NBTTagCompound;

@Configurable
public class PowerUpDeathKnell extends BasePowerUp
{
   private final static int DEFAULT_MAX_STRENGTH = 10;
   private final static int DEFAULT_MIN_LEVEL = 1;
   
   @Config(minFloat = 0f, maxFloat = 10f)
   private static float knellStrengthMultiplier = 0.6666667f;
   
   public PowerUpDeathKnell()
   {
      super(DEFAULT_MIN_LEVEL, DEFAULT_MAX_STRENGTH);
      HardcoreWither.CONFIG.register(this);   
   }

   @Override
   public void updateWither(IPowerUpEffect data)
   {}

   @Override
   public void witherDied(IPowerUpEffect data)
   {
      final EntityWither wither = data.getWither();
      wither.worldObj.newExplosion(wither, wither.posX, wither.posY + wither.getEyeHeight(), wither.posZ, 7.0F * knellStrengthMultiplier * data.getStrength(), false, wither.worldObj.getGameRules().getBoolean("mobGriefing"));
   }

   @Override
   public IPowerUpEffect getPowerUpEffect(EntityWither wither, int strength)
   {
      final BasePowerUpEffect data = new BasePowerUpEffect(wither, this);
      data.setStrength(strength);
      return data;
   }
   
   @Override
   public IPowerUpEffect restorePowerUp(EntityWither wither, NBTTagCompound stateNbt)
   { 
      final BasePowerUpEffect data = new BasePowerUpEffect(wither, this);
      data.deserializeNBT(stateNbt);
      return data;
   }
}
