package thor12022.hardcorewither.wither;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.event.world.WorldEvent.Load;
import net.minecraftforge.event.world.WorldEvent.Unload;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import thor12022.hardcorewither.HardcoreWither;
import thor12022.hardcorewither.ModInformation;
import thor12022.hardcorewither.config.Config;
import thor12022.hardcorewither.config.Configurable;
import thor12022.hardcorewither.enchantment.EnchantmentRegistry;
import thor12022.hardcorewither.player.PlayerData;
import thor12022.hardcorewither.util.I18n;

@Configurable(sectionName="Wither")
public class WitherHandler
{
   @Config(comment="Will enable the Looting enchant on weapons to affect the Wither's drops")
   private boolean witherLooting = true;
   
   @Config
   private boolean netherStarLooting = true;
   
   @Config(minFloat = 0f, comment="Scales the amount of reward in relation to Wither strength (non-linear)")
   private float lootingLevelMultiplier = 2f;
   
   @Config(minFloat=0f)
   private float netherStarLootingMultiplier = .5f;
   
   private WitherWorldData saveData = null;
   
   public WitherHandler()
   {
      HardcoreWither.CONFIG.register(this);
      HardcoreWither.CONFIG.register(WitherAffinityHelper.class);
      MinecraftForge.EVENT_BUS.register(this);
      
      PowerUpHelper.initialize();
      WitherData.register();
      
   }
   
   @SubscribeEvent
   public void onWorldLoad(Load event)
   {
      if(saveData == null && !event.getWorld().isRemote)
      {
         saveData = WitherWorldData.loadWitherWorldSavedData(event.getWorld());
      }
   }
   
   @SubscribeEvent
   public void onWorldUnload(@SuppressWarnings("unused") Unload event)
   {
      if(saveData != null)
      {
         saveData.markDirty();
         saveData = null;
      }
   }
   
   @SubscribeEvent
   public void onMobConstructing(AttachCapabilitiesEvent.Entity event)
   {
      if (event.getEntity() != null && event.getEntity().getClass() == EntityWither.class)
      {
         event.addCapability(WitherData.NBT_TAG, WitherData.getWitherDataProvider((EntityWither)event.getEntity()));
      }
   }
   
   @SubscribeEvent
   public void onSpawnMob(EntityJoinWorldEvent event)
   {
      if(!event.getWorld().isRemote)
      {
         if (event.getEntity() != null && event.getEntity().getClass() == EntityWither.class)
         {
            EntityWither theWither = (EntityWither)event.getEntity();
            WitherData witherData = WitherData.getWitherData(theWither);
            if(!witherData.isPoweredUp())
            {
               List<EntityPlayer> nearbyPlayers = theWither.worldObj.getEntitiesWithinAABB(EntityPlayer.class, theWither.getEntityBoundingBox().expand(64.0D, 64.0D, 64.0D));
               double powerUpSize = 0.0;
               for (EntityPlayer player : nearbyPlayers)
               {
                  final PlayerData data = PlayerData.getPlayerData(player);
                  powerUpSize += data.wasAtWitherSpawn();
               }
               if(saveData != null)
               {
                  if(powerUpSize > saveData.getLargestPowerUp())
                  {
                     saveData.setLargestPowerUp((int) Math.round(powerUpSize));
                  }
                  if(nearbyPlayers.size() == 0)
                  {
                     saveData.setLargestPowerUp(saveData.getLargestPowerUp() + 1);
                     powerUpSize = saveData.getLargestPowerUp();
                  }
               }
               else
               {
                  HardcoreWither.LOGGER.error("Cannot access WitherWorldData, did not load properly. Not all data may save.");
               }
               PowerUpHelper.powerUpWither(witherData, (int)Math.round(powerUpSize));   
            }
         }
      }
   }
   
   @SubscribeEvent
   public void onLivingUpdate(LivingUpdateEvent event)
   {
      if(!event.getEntity().worldObj.isRemote)
      {
         if (event.getEntityLiving() != null && event.getEntityLiving().getClass() == EntityWither.class)
         {
            WitherData data = WitherData.getWitherData((EntityWither) event.getEntityLiving());
            if(data != null)
            {
               data.update();
            }
         }
      }
   }
   
   @SubscribeEvent
   public void onLivingHurtEvent(LivingHurtEvent event)
   {
      if(event.getEntity() instanceof EntityWither && 
         event.getSource().getEntity() instanceof EntityPlayer)
      {
         EntityPlayer player = (EntityPlayer) event.getSource().getEntity();
         ItemStack weaponStack = player.getHeldItem(EnumHand.MAIN_HAND);
         if(weaponStack != null && HardcoreWither.API.getWitherAffinityRegistry().isRegistered(weaponStack.getItem()))
         {
            Map<Enchantment, Integer> enchantMap = EnchantmentHelper.getEnchantments(weaponStack);
            Integer witherAffinityLevel = enchantMap.get(EnchantmentRegistry.witherAffinity);
            
            if(event.getEntityLiving().getHealth() <= event.getAmount())
            {
               if(witherAffinityLevel == null)
               {
                  enchantMap.put(EnchantmentRegistry.witherAffinity, 1);
                  EnchantmentHelper.setEnchantments(enchantMap, weaponStack);
               }
            }
            else if (witherAffinityLevel != null)
            {
               WitherAffinityHelper.setWitherAffinityXp(weaponStack, WitherAffinityHelper.getWitherAffinityXp(weaponStack) + Math.round(event.getAmount()));
                           
               if(WitherAffinityHelper.shouldWitherAffinityLevelUp(weaponStack, witherAffinityLevel))
               {
                  enchantMap.put(EnchantmentRegistry.witherAffinity, witherAffinityLevel + 1);
                  EnchantmentHelper.setEnchantments(enchantMap, weaponStack);
               }
               
               // Roll under the target for 2nd Ed flashbacks
               if(HardcoreWither.RAND.nextFloat() < WitherAffinityHelper.chanceCalc(witherAffinityLevel))
               {
                  WitherData witherData = WitherData.getWitherData((EntityWither) event.getEntity());
                  if(witherData != null)
                  {
                     PowerUpHelper.reduceWitherPowerUp(witherData);
                  }
               }
            }
         }
      }
   }
   
   @SubscribeEvent
   public void witherLootDrops(LivingDropsEvent event)
   {
      if(!event.getEntity().worldObj.isRemote && event.getEntityLiving() != null && event.getEntityLiving().getClass() == EntityWither.class)
      {
         WitherData witherData = WitherData.getWitherData((EntityWither) event.getEntityLiving());
         // If we have not seen this Wither Die yet
         if(witherData != null && !witherData.isPendingLootDrops())
         {
            witherData.setPendingLootDrops(true);
            // Recalculate the looting based upon Wither strength 
            int lootingLevel = (witherLooting ? event.getLootingLevel() : 0) + (int)Math.round( Math.log10(witherData.getStrength() + 1) * lootingLevelMultiplier);
            // and resend the drops event
            if(!ForgeHooks.onLivingDrops(event.getEntityLiving(), event.getSource(), (ArrayList<EntityItem>)event.getDrops(), lootingLevel, event.isRecentlyHit()))
            {
               //then do the same thing EntityLivingBase does
               for (EntityItem item : event.getDrops())
               {
                  event.getEntityLiving().worldObj.spawnEntityInWorld(item);
               }
               //! @note I don't like doing this, as far as the Forge Event manager is concerned the death event was cancelled
               //!   and that could have some side effects. However, the only other way I can think of at the moment is to 
               //!   use Reflection to modify the lootingLevel field of the LivingDropsEvent, which seems even worse.
            }
            // then cancel this one
            event.setCanceled(true);
         }
         // This is the resent event
         else if(witherData != null)
         {
            witherData.setPendingLootDrops(false);
            
            if (netherStarLooting && event.getLootingLevel() > 0)
            {
                int j = 0;
   
                j += Math.round(Math.abs(HardcoreWither.RAND.nextGaussian() * event.getLootingLevel() * netherStarLootingMultiplier));
   
                if( j > 0)
                {
                   EntityItem entityItem = new EntityItem(event.getEntityLiving().worldObj, event.getEntityLiving().posX, event.getEntityLiving().posY, event.getEntityLiving().posZ, new ItemStack(Items.NETHER_STAR, j, 0));
                   entityItem.setPickupDelay(10);
                   event.getDrops().add(entityItem);
                }
            }
         }
      }
   }
   
   @SubscribeEvent
   public void onEntityDieing(LivingDeathEvent event)
   {
      if(!event.getEntity().worldObj.isRemote)
      {
         if (event.getEntityLiving() != null && event.getEntityLiving().getClass() == EntityWither.class)
         {
            WitherData witherData = WitherData.getWitherData((EntityWither) event.getEntityLiving());
            if(witherData != null)
            {
               witherData.died();
               
               List<EntityPlayer> nearbyPlayers = event.getEntity().worldObj.getEntitiesWithinAABB(EntityPlayer.class, event.getEntity().getEntityBoundingBox().expand(64.0D, 64.0D, 64.0D));
               for (EntityPlayer player : nearbyPlayers)
               {
                  final PlayerData data = PlayerData.getPlayerData(player);
                  data.wasAtWitherSpawn();
                  player.addChatMessage(new TextComponentString(I18n.localize("info." + ModInformation.ID + ".chat.wither-experience")));
               }
            }
         }
      }
   }
   

   @SubscribeEvent
   public void onTooltipEvent(ItemTooltipEvent event)
   {
      int witherAffinityLevel = EnchantmentHelper.getEnchantmentLevel(EnchantmentRegistry.witherAffinity, event.getItemStack());
      if(witherAffinityLevel > 0)
      {
         int witherAffinityXp = WitherAffinityHelper.getWitherAffinityXp(event.getItemStack());
         String witherAffinityLine = I18n.localize("tooltip." + ModInformation.ID + ".witherAffinity") + ": " +  witherAffinityXp + "/" + WitherAffinityHelper.xpCalc(witherAffinityLevel);
         
         event.getToolTip().add(witherAffinityLine);
         event.getToolTip().add("");         
      }
   }
}
