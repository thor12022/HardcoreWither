package thor12022.hardcorewither.wither;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraft.world.WorldSavedData;
import thor12022.hardcorewither.HardcoreWither;
import thor12022.hardcorewither.ModInformation;

public class WitherWorldData extends WorldSavedData
{
   private static final int      NBT_FORMAT  = 2;
   private static final String   NAME        = ModInformation.ID + "_witherData";
   
   public static WitherWorldData loadWitherWorldSavedData(World world)
   {
      WitherWorldData data = null;
      if(world.provider.getDimension() == 0 && world.getMapStorage() != null)
      {
         try
         {
            data = (WitherWorldData)world.getMapStorage().getOrLoadData(WitherWorldData.class, NAME);

            if(data == null)
            {
               data = new WitherWorldData(NAME);
               data.markDirty();
               world.getMapStorage().setData(NAME, data);
            }
         }
         catch(ClassCastException e)
         {
            HardcoreWither.LOGGER.error(e);
            HardcoreWither.LOGGER.warn("Cannot properly get WitherWorldData");
         }
      }
      return data;
   }
   
   private int largestPowerUp = 0;
   
   public WitherWorldData(String name)
   {
      super(name);
   }
   
   /**
    * ROOT
    * +--formatVersion : n
    * +--largestPowerUp : n  
    */
   @Override
   public NBTTagCompound writeToNBT(NBTTagCompound nbt)
   {
      nbt.setInteger("formatVersion", NBT_FORMAT);
      nbt.setInteger("largestPowerUp", largestPowerUp );
      return nbt;
   }

   @Override
   public void readFromNBT(NBTTagCompound nbt)
   {
      int formatVersion = nbt.getInteger("formatVersion");
      if(formatVersion < NBT_FORMAT)
      {
         HardcoreWither.LOGGER.warn("Detected old version of saved data. Withers' abilities will not convert to ", ModInformation.VERSION, " from older versions");
      }
      largestPowerUp = nbt.getInteger("largestPowerUp");
   }
   
   int getLargestPowerUp()
   {
      return largestPowerUp;
   }

   void setLargestPowerUp(final int size)
   {
      largestPowerUp = size;
   }
}
