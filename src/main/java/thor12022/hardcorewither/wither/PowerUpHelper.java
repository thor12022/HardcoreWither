package thor12022.hardcorewither.wither;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import thor12022.hardcorewither.wither.powerups.PowerUpBlazeMinionSpawner;
import thor12022.hardcorewither.wither.powerups.PowerUpDamageResistance;
import thor12022.hardcorewither.wither.powerups.PowerUpDeathKnell;
import thor12022.hardcorewither.wither.powerups.PowerUpGhastMinionSpawner;
import thor12022.hardcorewither.wither.powerups.PowerUpHealthBoost;
import thor12022.hardcorewither.wither.powerups.PowerUpLightning;
import thor12022.hardcorewither.wither.powerups.PowerUpSkeletonMinionSpawner;
import thor12022.hardcorewither.wither.powerups.PowerUpTeleport;
import thor12022.hardcorewither.HardcoreWither;
import thor12022.hardcorewither.api.IPowerUp;
import thor12022.hardcorewither.api.IPowerUpEffect;
import thor12022.hardcorewither.util.I18n;

class PowerUpHelper
{   
   static void initialize()
   {
      HardcoreWither.API.getPowerUpRegistry().register(new PowerUpBlazeMinionSpawner());
      HardcoreWither.API.getPowerUpRegistry().register(new PowerUpDeathKnell());
      HardcoreWither.API.getPowerUpRegistry().register(new PowerUpGhastMinionSpawner());
      HardcoreWither.API.getPowerUpRegistry().register(new PowerUpHealthBoost());
      HardcoreWither.API.getPowerUpRegistry().register(new PowerUpSkeletonMinionSpawner());
      HardcoreWither.API.getPowerUpRegistry().register(new PowerUpLightning());
      HardcoreWither.API.getPowerUpRegistry().register(new PowerUpTeleport());
      HardcoreWither.API.getPowerUpRegistry().register(new PowerUpDamageResistance());
      
      HardcoreWither.COMMAND.addSubcommand(spawnCommand);
   }
   
   static CommandBase spawnCommand = new CommandBase()
   {
      @Override
      public final String getCommandUsage(ICommandSender sender)
      {
         return "commands." + getCommandName() + ".usage";
      }
      @Override
      public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos pos)
      {
         List<String> powerUps = new ArrayList<>();
         for(ResourceLocation powerUp : HardcoreWither.API.getPowerUpRegistry().getKeys())
         {
            powerUps.add(powerUp.toString());
         }
         return powerUps;
      }

      @Override
      public final String getCommandName()
      {
         return "spawn";
      }

      @Override
      public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws WrongUsageException
      {
         if( args.length < 1 )
         {
            throw new WrongUsageException(getCommandUsage(sender));
         }
         EntityWither spawnedWither = new EntityWither(sender.getEntityWorld());
         BlockPos coords = sender.getPosition();
         spawnedWither.setPosition(coords.getX(), coords.getY(), coords.getZ());
         spawnedWither.ignite();
         sender.getEntityWorld().spawnEntityInWorld(spawnedWither);
         try
         {
            WitherData witherData = WitherData.getWitherData(spawnedWither);
            if(witherData != null)
            {
               witherData.removeAllPowerUpEffects();
               for(String arg : args)
               {
                  String[] splitArg = arg.split(",");
                  String powerUpName = splitArg[0];
                  int powerUpStrength = splitArg.length > 1 ? parseInt(splitArg[1], 1) : 1;
                  IPowerUp powerUp = HardcoreWither.API.getPowerUpRegistry().getValue(new ResourceLocation(powerUpName));
                  if(powerUp == null)
                  {
                     HardcoreWither.LOGGER.debug("PowerUp: " + powerUpName + "not valid");
                     throw new WrongUsageException(getCommandUsage(sender));
                  }
                  witherData.addPowerUpEffect(powerUp, powerUpStrength);
               }
            }
         }
         catch(Exception excp)
         {
            spawnedWither.worldObj.removeEntity(spawnedWither);
            // ok, so this is kinda the Lazy Man's way of making sure nothing really goes wrong
            HardcoreWither.LOGGER.debug("PowerUp Command Formatting Error (probably) not accounted for " + excp);
            throw new WrongUsageException(getCommandUsage(sender));
         }
      }

      @Override
      public int getRequiredPermissionLevel()
      {
          return 2;
      }
   };

   
   /**
    * Apply an amount of Power Ups to a certain Wither
    * @param witherData apply power ups to this
    * @param sizeOfPowerUp this many levels of Power Ups
    * @note wither should not have had Power Ups applied to it already
    */
   static void powerUpWither(WitherData witherData, int sizeOfPowerUp)
   {
      if(witherData.getActivePowerUpEffectsNum() == 0)
      {
         int usedStrength = 0;
         //! @todo investigate HardcoreWither.API.getPowerUpRegistry().getRandomObject(random)         
         List<IPowerUp> validPowerUps = HardcoreWither.API.getPowerUpRegistry().getValues();
         validPowerUps.removeIf(powerUp -> powerUp.minWitherLevel() > sizeOfPowerUp);
         validPowerUps.removeIf(powerUp -> powerUp.canApply(witherData.getWither()));
         while(usedStrength < sizeOfPowerUp && validPowerUps.size() > 0)
         {
            int powerUpIndex = HardcoreWither.RAND.nextInt(validPowerUps.size());
            IPowerUp powerUp = validPowerUps.get(powerUpIndex);
            IPowerUpEffect powerUpEffect = witherData.getActivePowerUpEffect(powerUp);
            if(powerUpEffect != null)
            {
               int currentStength = powerUpEffect.getStrength();
               if(currentStength < powerUp.maxStrength())
               {
                  powerUpEffect.setStrength(currentStength + 1);
                  ++usedStrength;
                  HardcoreWither.LOGGER.debug("Increasing power of " + powerUp.getClass());
               }
               else
               {
                  validPowerUps.remove(powerUpIndex);
               }
            }
            // If this is a new powerup for this Wither
            else
            {
               witherData.addPowerUpEffect(powerUp, 1);
               //! @todo this line should probably be different
               usedStrength += powerUp.minWitherLevel() > 0 ? powerUp.minWitherLevel() : 1;
               HardcoreWither.LOGGER.debug("Adding " + powerUp.getRegistryName());
            }
         }
      }
      else
      {
         HardcoreWither.LOGGER.debug("Attempting to re-powerup Wither");
      }
      witherData.setPoweredUp(true);
   }
   
   /**
    * Randomly removes one level from a random PowerUp the Wither has
    * @param witherData apply power ups to this
    * @pre wither should have had Power Ups applied to it already
    */
   static public void reduceWitherPowerUp(WitherData witherData)
   {
      IPowerUpEffect powerUpEffect = witherData.getRandomActivePowerUpEffect();
      
      final int currentStrength = powerUpEffect.getStrength();
      if(currentStrength == 1)
      {
         witherData.removePowerUpEffect(powerUpEffect);
      }
      else
      {
         powerUpEffect.setStrength(currentStrength - 1);
      }
   }
}
