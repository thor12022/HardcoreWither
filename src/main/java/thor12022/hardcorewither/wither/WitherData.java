package thor12022.hardcorewither.wither;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.Nonnull;

import net.minecraft.entity.boss.EntityWither;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import thor12022.hardcorewither.HardcoreWither;
import thor12022.hardcorewither.api.IPowerUp;
import thor12022.hardcorewither.api.IPowerUpEffect;

public class WitherData
{
   static final ResourceLocation NBT_TAG  =  new ResourceLocation("witherData");
   
   private static final String NBT_EFFECT_NAME_TAG = "name";
   private static final String NBT_EFFECT_TAG      = "effect";
   
   @CapabilityInject(WitherData.class)
   public static Capability<WitherData> CAPABILITY;
   
   private final Set<IPowerUpEffect>   activePowerUps       =  new HashSet<>();
   private boolean                     hasDied              =  false;
   private boolean                     isPendingLootDrops   =  false;
   private boolean                     isPoweredUp          =  false;
   private final EntityWither          ownerWither;

   static void register()
   {
      CapabilityManager.INSTANCE.register(WitherData.class,new Capability.IStorage<WitherData>()
      {
         @Override
         public NBTBase writeNBT(Capability<WitherData> capability, WitherData instance, EnumFacing side)
         {
            return null;
         }

         @Override
         public void readNBT(Capability<WitherData> capability, WitherData instance, EnumFacing side, NBTBase base)
         {}
     }, new Callable<WitherData>()
     {
         @Override
         public WitherData call() throws Exception
         {
             return new WitherData(null);
         }
     });
   }
   
   static ICapabilitySerializable<NBTTagCompound> getWitherDataProvider(EntityWither Wither)
   {
      return new ICapabilitySerializable<NBTTagCompound>()
      {
         WitherData cap = new WitherData(Wither);

         @Override
         public boolean hasCapability(Capability<?> capability, EnumFacing facing)
         {
            return capability == CAPABILITY;
         }

         @Override
         public <T> T getCapability(Capability<T> capability, EnumFacing facing)
         {
            if(capability == CAPABILITY)
            {
               return CAPABILITY.cast(cap);
            }
            return null;
         }

         @Override
         public NBTTagCompound serializeNBT()
         {
            return cap.serializeNBT();
         }

         @Override
         public void deserializeNBT(NBTTagCompound nbt)
         {
            cap.deserializeNBT(nbt);
         }
   
      };
   }
   
   public static WitherData getWitherData(EntityWither wither)
   {      
      if(wither.hasCapability(CAPABILITY, null))
      {
         return wither.getCapability(CAPABILITY, null);
      }
      return null;
   } 
   
   WitherData(EntityWither wither)
   {
      ownerWither = wither;
   }

   int getActivePowerUpEffectsNum()
   {
      return activePowerUps.size();
   }
   
   IPowerUpEffect getRandomActivePowerUpEffect()
   {
      return (IPowerUpEffect)activePowerUps.toArray()[HardcoreWither.RAND.nextInt(activePowerUps.size())];
   }
   
   /**
    * @param powerUp
    * @returns null if PowerUp not active
    * @todo efficiency?
    */
   IPowerUpEffect getActivePowerUpEffect(IPowerUp powerUp)
   {
      for(IPowerUpEffect effect : activePowerUps)
      {
         if(effect.getCreator() == powerUp)
         {
            return effect;
         }
      }
      return null;
   }
   
   /**
    * @returns true if successful, false if PowerUpEffect already exists (or other error)
    * @todo @nonnull, really?
    */
   boolean addPowerUpEffect(@Nonnull IPowerUp powerUp, int powerUpStrength)
   {
      if(getActivePowerUpEffect(powerUp) == null)
      {
         IPowerUpEffect powerUpEffect =  powerUp.getPowerUpEffect(ownerWither, powerUpStrength);
         if(powerUpEffect != null)
         {
            activePowerUps.add(powerUpEffect);
            return true;
         }
      }
      return false;         
   }
   
   /**
    * @param 
    * @returns true if successful, false if PowerUpEffect didn't exist
    */
   boolean removePowerUpEffect(IPowerUpEffect powerUpEffect)
   {
      return activePowerUps.remove(powerUpEffect);
   }
   
   void removeAllPowerUpEffects()
   {
      activePowerUps.clear();
   }
   
   boolean isPendingLootDrops()
   {
      return isPendingLootDrops;
   }

   void setPendingLootDrops(boolean isPendingLootDrops)
   {
      this.isPendingLootDrops = isPendingLootDrops;
   }

   /**
    * @returns the total strength of all PowerUps
    * @note recalculated every call
    * @todo cache this?
    */
   int getStrength()
   {
      // Alright, so this is an interesting thing. But. . . a bit much.
      final AtomicInteger strength  = new AtomicInteger(0);
      activePowerUps.forEach(effect -> strength.getAndAdd(effect.getStrength()));
      return strength.get();
      
      //! @todo compare these with benchmarks, just for the heck of it
      
//      int strength = 0;
//      for(IPowerUpEffect effect : activePowerUps)
//      {
//         strength += effect.getStrength();
//      }
//      return strength;
   }

   public boolean isPoweredUp()
   {
      return isPoweredUp;
   }

   public void setPoweredUp(boolean isPoweredUp)
   {
      this.isPoweredUp = isPoweredUp;
   }

   public EntityWither getWither()
   {
      return ownerWither;
   }

   /**
    * tick update
    */
   void update()
   {
      for(IPowerUpEffect powerUp : activePowerUps)
      {
         try
         {
            powerUp.updateEffect();
         }
         // If any exception occurred, assume it's this powerup's fault
         catch(Exception e)
         {            
            HardcoreWither.LOGGER.error(e);
            HardcoreWither.LOGGER.error("Error Occured while updating PowerUp " + powerUp.getCreator().getRegistryName() + ", removing.");
            activePowerUps.remove(powerUp);
            // skip the rest of this update, or suffer the wrath of ConcurrentModificationException,
            //    alternatively, switch the for loop to use Iterators, then it would be fine
            break;
         }
      }
   }
   
   /**
    * died update
    */
   void died()
   {
      for(IPowerUpEffect powerUp : activePowerUps)
      {
         powerUp.onDied();
      }
   }
      
   public NBTTagCompound serializeNBT()
   {
      NBTTagCompound witherDataTag = new NBTTagCompound();
      
      NBTTagList powerUpEffectsTagList = new NBTTagList();
      
      for(IPowerUpEffect effect : activePowerUps)
      {
         NBTTagCompound effectTag = new NBTTagCompound();
         effectTag.setString(NBT_EFFECT_NAME_TAG, effect.getCreator().getRegistryName().toString());
         effectTag.setTag(NBT_EFFECT_TAG, effect.serializeNBT());
         powerUpEffectsTagList.appendTag(effectTag);
      }
      witherDataTag.setTag("powerUpEffectsList", powerUpEffectsTagList);
      witherDataTag.setBoolean("isPoweredUp", isPoweredUp);
      witherDataTag.setBoolean("hasDied", hasDied);
      witherDataTag.setBoolean("isPendingLootDrops", isPendingLootDrops);
      return witherDataTag;
   }
   
   public void deserializeNBT(NBTTagCompound compound)
   {
      NBTTagList powerUpEffectsTagList = compound.getTagList("powerUpEffectsList", (new NBTTagCompound()).getId());
      isPoweredUp = compound.getBoolean("isPoweredUp");
      hasDied = compound.getBoolean("hasDied");
      isPendingLootDrops = compound.getBoolean("isPendingLootDrops");
      for(int i = 0; i < powerUpEffectsTagList.tagCount(); ++i)
      {
         try
         {
            NBTTagCompound powerUpEffectTag = powerUpEffectsTagList.getCompoundTagAt(i);
            IPowerUp powerUp = HardcoreWither.API.getPowerUpRegistry().getValue(new ResourceLocation(powerUpEffectTag.getString(NBT_EFFECT_NAME_TAG)));
            IPowerUpEffect effect  = powerUp.restorePowerUp(ownerWither, powerUpEffectTag.getCompoundTag(NBT_EFFECT_TAG));
            activePowerUps.add(effect);
         }
         catch(NullPointerException e)
         {
            HardcoreWither.LOGGER.warn(e);
            HardcoreWither.LOGGER.warn("Problem loading " + powerUpEffectsTagList.getCompoundTagAt(i).getId() + " NBT for EntityWither, " + ownerWither.getUniqueID());
         }
      }
   }
}

