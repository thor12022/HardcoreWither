package thor12022.hardcorewither.command;

import thor12022.hardcorewither.ModInformation;
import thor12022.hardcorewither.util.I18n;
import net.minecraft.command.CommandHandler;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraftforge.server.command.CommandTreeBase;

public class CommandManager extends CommandTreeBase
{
   public void register(CommandHandler commandHandler)
   {
      commandHandler.registerCommand(this);
   }
   
   @Override
   public String getCommandName()
   {
      return ModInformation.CHANNEL;
   }

   @Override
   public String getCommandUsage(ICommandSender sender)
   {
      String text = I18n.localize("command." + getCommandName() + ".usage") + "\n";
      for(ICommand subCommand : getSubCommands())
      {
         text += subCommand.getCommandName() + "\n";
      }
      return text;
   }
}
