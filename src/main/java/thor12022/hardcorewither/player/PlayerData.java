package thor12022.hardcorewither.player;

import java.util.concurrent.Callable;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;


/**
 * 
 * @todo this needs a better interface, wasAtWitherSpawn is terribly named and has multiple uses
 *
 */
public class PlayerData
{
   static final ResourceLocation NBT_TAG  =  new ResourceLocation("playerData");
   
   private static final String NBT_PLAYER_XP = "playerXp";
   
   @CapabilityInject(PlayerData.class)
   public static Capability<PlayerData> CAPABILITY;
   
   static void register()
   {
      CapabilityManager.INSTANCE.register(PlayerData.class,new Capability.IStorage<PlayerData>()
      {
         @Override
         public NBTBase writeNBT(Capability<PlayerData> capability, PlayerData instance, EnumFacing side)
         {
            return null;
         }

         @Override
         public void readNBT(Capability<PlayerData> capability, PlayerData instance, EnumFacing side, NBTBase base)
         {}
     }, new Callable<PlayerData>()
     {
         @Override
         public PlayerData call() throws Exception
         {
             return new PlayerData();
         }
     });
   }
   
   static ICapabilitySerializable<NBTTagCompound> getPlayerDataProvider()
   {
      return new ICapabilitySerializable<NBTTagCompound>()
      {
         PlayerData cap = new PlayerData();

         @Override
         public boolean hasCapability(Capability<?> capability, EnumFacing facing)
         {
            return capability == CAPABILITY;
         }

         @Override
         public <T> T getCapability(Capability<T> capability, EnumFacing facing)
         {
            if(capability == CAPABILITY)
            {
               return CAPABILITY.cast(cap);
            }
            return null;
         }

         @Override
         public NBTTagCompound serializeNBT()
         {
            return cap.serializeNBT();
         }

         @Override
         public void deserializeNBT(NBTTagCompound nbt)
         {
            cap.deserializeNBT(nbt);
         }
   
      };
   }
   
   public static PlayerData getPlayerData(EntityPlayer player)
   {      
      if(player.hasCapability(CAPABILITY, null))
      {
         return player.getCapability(CAPABILITY, null);
      }
      return null;
   } 
   
   double playerXp;
 
   //! @todo config
   public double wasAtWitherSpawn()
   {
      return addWitherExperience(1.0);
   }
   
   private double addWitherExperience(double wxp)
   {
      playerXp += wxp;
      return playerXp;
   }
   
   public NBTTagCompound serializeNBT()
   {
      NBTTagCompound nbt = new NBTTagCompound();
      nbt.setDouble(NBT_PLAYER_XP, playerXp);
      return nbt;
   }
   
   public void deserializeNBT(NBTTagCompound nbt)
   {
      playerXp = nbt.getDouble(NBT_PLAYER_XP);
   }
   
   void clone(PlayerData source)
   {
      playerXp = source.playerXp;
   }
}
