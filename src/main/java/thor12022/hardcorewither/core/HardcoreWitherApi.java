package thor12022.hardcorewither.core;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.FMLControlledNamespacedRegistry;
import net.minecraftforge.fml.common.registry.PersistentRegistryManager;
import thor12022.hardcorewither.ModInformation;
import thor12022.hardcorewither.api.IHardcoreWitherApi;
import thor12022.hardcorewither.api.IPowerUp;
import thor12022.hardcorewither.api.IWitherAffinityRegistry;

public class HardcoreWitherApi implements IHardcoreWitherApi
{
   private final FMLControlledNamespacedRegistry<IPowerUp>  powerUpRegistry         = PersistentRegistryManager.createRegistry(new ResourceLocation(ModInformation.ID + ":powerUp"), IPowerUp.class, null, 0, Integer.MAX_VALUE >> 5, false, null, null, null);
   
   private final WitherAffinityRegistry                     witherAffinityRegistry  =  new WitherAffinityRegistry();
   
   @Override
   public FMLControlledNamespacedRegistry<IPowerUp> getPowerUpRegistry()
   {
      return powerUpRegistry;
   }

   @Override
   public IWitherAffinityRegistry getWitherAffinityRegistry()
   {
      return witherAffinityRegistry;
   }

}
