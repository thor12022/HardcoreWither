package thor12022.hardcorewither.core;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import net.minecraft.item.Item;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.LoaderState;
import thor12022.hardcorewither.api.IWitherAffinityRegistry;

public class WitherAffinityRegistry implements IWitherAffinityRegistry
{
   final private Collection<Item> items =  new HashSet<>();

   /**
    * @return false if registration failed, or duplicate Item
    * @pre The server has not been started yet (Constructing, Preinit, or init)
    * @todo what about sub-items?
    */
   @Override
   public boolean register(Item item)
   {
      if(!Loader.instance().hasReachedState(LoaderState.SERVER_ABOUT_TO_START) && item != null)
      {
         return items.add(item);
      }
      
      return false;
   }
   
   /**
    * @return an unmodifiable collection of all Items that have been registered
    */
   @Override
   public Collection<Item> getAll()
   {
      return  Collections.unmodifiableCollection(items);
   }
   
   /**
    * @return true if the given Item has been registered
    */
   @Override
   public boolean isRegistered(Item item)
   {
      return items.contains(item);
   }
      
}
