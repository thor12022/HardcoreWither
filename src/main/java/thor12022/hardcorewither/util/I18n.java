package thor12022.hardcorewither.util;

import net.minecraftforge.fml.common.SidedProxy;

public class I18n 
{
   @SidedProxy
   public static CommonProxy proxy;
   
   public static class CommonProxy
   {
      String Localize(String key)
      {
         return key;
      }
      
      @SuppressWarnings("unused")
      String Format(String key, Object[] parameters)
      {
         return key;
      }
      
      @SuppressWarnings("unused")
      boolean hasKey(String key)
      {
         return false; 
      }
   }
   
   public static class ServerProxy extends CommonProxy
   {}
   
   public static class ClientProxy extends CommonProxy
   {
      @Override
      String Localize(String key)
      {
         return net.minecraft.client.resources.I18n.format(key, new Object[0]);
      }
      
      @Override
      String Format(String key, Object[] parameters)
      {
         return net.minecraft.client.resources.I18n.format(key, parameters);
      }
      
      @Override
      boolean hasKey(String key)
      {
         return true; 
      }
   }
   
   public static String localize(String key)
   {
      return proxy.Localize(key);
   }
   
   public static String Format(String key, Object[] parameters)
   {
      return proxy.Format(key, parameters);
   }
   
   public static boolean hasKey(String key)
   {
      return proxy.hasKey(key);
   }
}
